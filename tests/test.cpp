#define CATCH_CONFIG_MAIN 
#include <catch.hpp>

#include <core/core.hpp>

TEST_CASE("test tests", "[core]") {
    imageRT::Core::Core cr;
    REQUIRE(cr.getFileExtention("d.ext") == ".ext");
    REQUIRE(cr.getFileExtention("") == "");
    REQUIRE(cr.getFileExtention("ext") == "");
    REQUIRE(cr.getFileExtention("ext.") == ".");
   
}