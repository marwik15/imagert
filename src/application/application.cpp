#pragma once

/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/
#define STB_IMAGE_IMPLEMENTATION
#include <application/application.hpp>

namespace imageRT {
	namespace application {
		
			void Application::console(bool show) {
			#ifdef WIN32
				if(show)
					ShowWindow(GetConsoleWindow(), SW_SHOW);
				else 
					ShowWindow(GetConsoleWindow(), SW_HIDE);
			#endif // WIN32
			}
			
			void Application::start() {
				//Core::Lua::Lua lua;
				//lua.run();
				_scenes.push_back(Scene());
				Gui::GUI gui(_scenes[0]);
				gui.show();
			}
	} //namespace Application
} //namespace imageRT