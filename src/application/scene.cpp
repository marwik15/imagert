#pragma once

/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/

#include <application/scene.hpp>

namespace imageRT {
	namespace application{

		Scene::~Scene(){
			exit();
		}

		void Scene::setFilelist(std::vector<std::vector<imageRT::DB::DBreturnedTab>>& fileList) {
			this->fileList = fileList;
		}

		std::vector<std::vector<imageRT::DB::DBreturnedTab>>& Scene::getFileList() {
			return fileList;
		}
		
		void Scene::addModelToDraw(imageRT::Core::Data::Model* mdl) {
			this->modelsToDraw.push_back(mdl);

		}
		
		void Scene::addObjectToProject(std::string path) {
			imageRT::Core::Data::Model* mdl = new imageRT::Core::Data::Model;
			mdl->loadModel(path);
			addModelToDraw(mdl);
		}

		std::vector<imageRT::Core::Data::Model*> Scene::getModelsToDraw() {
			return modelsToDraw;
		}

		void Scene::openProject(std::string  path) {
			OPTICK_EVENT("open project");
			char* sql = "SELECT * from file";
			
			db.openDB(path);
			db.checkConnetionError();
			db.sendSQLquery(sql);
			db.checkSQLqueryError();
				

			setFilelist(db.getData());

			for (auto n : getFileList()) {
				imageRT::Core::Data::Model* mdl = new imageRT::Core::Data::Model;
				std::string p = path + "\\..\\" + n[1].argv;
				mdl->loadModel(p);
				addModelToDraw(mdl);
			}
		}

		void Scene::exit() {
			db.closeDB();
		}
		
	} //namespace Application
} //namespace imageRT