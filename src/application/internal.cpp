#pragma once

/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/

#include <application/internal.hpp>

namespace imageRT {
	namespace application {

		void Internal::fileBoxOpenProject(imageRT::application::Scene& scene) {
				
			nana::filebox picker{ nullptr, true };

			auto paths = picker.show();

			if (paths.empty()) {
				std::cout << "Cancelled" << std::endl;
			} else {
				for (auto& p : paths) {
					std::cout << "Selected file:" << p << std::endl;
					if (getFileExtention(p.string()) == ".imagert") {
						scene.openProject(p.string());
					} else {
						scene.addObjectToProject(p.string());
					}
				}
			}
		}
	} //namespace application
} //namespace imageRT