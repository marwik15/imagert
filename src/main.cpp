/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/

#include <string>
#include <iostream>
#include <application/application.hpp>

using namespace imageRT;

int main() {
	application::Application ImageRT;
	ImageRT.console(true);
	ImageRT.start();

	return 1;
}         