#pragma once

/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/

#include <gui/gui.hpp>

namespace imageRT {
    namespace Gui{
            void GUI::show() {
                using namespace nana;
                imageRT::Renderer::Opengl::openGLRenderer render;

                #ifdef ImageRT_VULKAN
                    Renderer::Vulkan::VulkanRenderer vkRenderer;
                    vkRenderer.rendererInit();
                #endif //ImageRT_VULKAN

                nana::API::zoom_window(fm, true);
               
                fm.caption(L"Hello, ImageRT!");
                
                mn.create(fm);
                mn.bgcolor(colors::dark_gray);
                menubar();

                place place_(fm);
                nana::panel<true> dockContainer(fm);
                dockContainer.bgcolor(nana::colors::dim_gray);

                place_.div("<vert <menubar weight=28><dockContainer>>");
                place_.field("menubar") << mn ;
                place_.field("dockContainer")<< dockContainer;
                place_.collocate();

                nana::nested_form viewport(fm, rectangle{ 10, 10, 900, 600 }, form::appear::bald<>());
                nana::nested_form tools(fm, rectangle{ 0, 0, 70, 600 }, form::appear::bald<>());
                nana::nested_form treeView(fm, rectangle{ 0, 0, 200, 600 }, form::appear::bald<>());
                nana::nested_form propertiesPanel(fm, rectangle{ 0, 0, 200, 600 }, form::appear::bald<>());
                nana::nested_form assetsPanel(fm, rectangle{ 0, 0, 1000, 300 }, form::appear::bald<>());
                assetsPanel.bgcolor(nana::colors::dark_red);
                treeView.bgcolor(nana::colors::dark_red);


                nana::place pPlace;
                pPlace.bind(propertiesPanel);
                nana::label lb1(propertiesPanel, true);
                nana::textbox t1(propertiesPanel,"-");
                nana::textbox t2(propertiesPanel,"-");
                nana::textbox t3(propertiesPanel,"-");
                lb1.caption("position");
                

                nana::label lb2(propertiesPanel, true);
                nana::textbox t4(propertiesPanel, "-");
                nana::textbox t5(propertiesPanel, "-");
                nana::textbox t6(propertiesPanel, "-");
                lb2.caption("scale");

                nana::label lb3(propertiesPanel, true);
                nana::textbox t7(propertiesPanel, "-");
                nana::textbox t8(propertiesPanel, "-");
                nana::textbox t9(propertiesPanel, "-");
                lb3.caption("rotation");

                nana::label lbTitle(propertiesPanel, true);
                lbTitle.caption("properties panel");

                pPlace.div("<vert <weight=20 lbTitle> <weight=20 pos><weight=20 scale><weight=20 rotation>>");
                pPlace["lbTitle"] << lbTitle;
                pPlace["pos"] << lb1<<t1<<t2<<t3;
                pPlace["scale"] << lb2<< t4 << t5 << t6;
                pPlace["rotation"] << lb3<< t7 << t8 << t9;
                pPlace.collocate();

                tlsPnl.create(tools, rectangle(), true);
        
                nana::button btn1{ tools, "move" };
                nana::button btn2{ tools, "rotate" };
                nana::button btn3{ tools, "scale" };

                nana::button btn4{ tools, "add cube" };
                nana::button btn5{ tools, "remove all" };
                nana::button btn6{ tools, "load" };

                btn5.events().mouse_up([&]()
                    {
                        render.deleteAll();
                    });

                btn6.events().mouse_up ([&]()
                    {
                        fileBoxOpenProject(_scene);
                        render.setModelsToDraw(_scene.getModelsToDraw());
                        updateTreeBox(tree);
                    });
              
                tools.div("<vert <fit vert ctrl> |0 <fit vert tls>>");
                tools["ctrl"] << btn1<< btn2 << btn3;
                tools["tls"] << btn4<< btn5 << btn6;
                tools.collocate();


                tree.create(treeView, { 0, 0, 300, 300 });

                tree.events().selected([&](const arg_treebox& arg)
                    {
                        std::cout<<tree.selected().text()<<std::endl;
                    });

                dockLayout docks(dockContainer);
                docks.addpanel(&tools,PanelInfo(80,20,true));
                docks.addpanel(&viewport, PanelInfo(350, 20));
                docks.addpanel(&treeView, PanelInfo(treeView.size().width-100, 20));
                docks.addpanel(&propertiesPanel, PanelInfo(propertiesPanel.size().width, 20));
                docks.collocate();
                docks.showAll();

                render.rendererInit(viewport.native_handle());
               
                bool mouseAcitve = false;

                //render loop
                viewport.draw_through([&render]() mutable
                    {
                        render.renderUpdate();
                });
               
                viewport.events().destroy([&render]
                    {
                        render.destroy();
                });

                viewport.events().resized([&viewport, &render]
                    {
                        auto s = viewport.size();
                        render.setViewPortSize(s.width, s.height);
                        render.saveViewportDimentions(s.width, s.height);
                });

                viewport.events().mouse_down([&](const arg_mouse& arg)
                    {
                        clipCursor(makeClipAreaFromViewport(viewport));

                        if (arg.right_button) {
                            mouseAcitve = true;
                            fm.focus(); //keyboard events
                        }
                    });
                viewport.events().mouse_up([&](const arg_mouse& arg)
                    { 
                        showCursor(true);                     
                        releaseClipCursor();
                        mouseAcitve = false;
                        resetMouseRepeated();
                    });
                viewport.events().mouse_leave([&](const arg_mouse& arg)
                    {
                        //showCursor(true);
                    });

                bool firstMouse = true;

                int firstMx = 0;
                int firstMy = 0;
                
                viewport.events().mouse_move([&](const arg_mouse& arg)
                    {
                        if (mouseAcitve) {
                            int _x = arg.pos.x;
                            int _y = arg.pos.y;
                            auto repX = getMouseXrepeated();
                            auto repY = getMouseYrepeated();

                            int xx = 0;
                            int yy = 0;

                            if (firstMouse) {
                                firstMx = _x;
                                firstMy = _y;
                                firstMouse = false;
                            }
                          
                            if (repX != 0) {
                                xx = _x + (repX * viewport.size().width);
                            } else {
                                xx = _x;
                            }

                            if (repY != 0) {
                                yy = _y - (repY * viewport.size().width);
                            } else {
                                yy = _y;
                            }

                            render.setMousePos(xx, yy);
                            moveCursorToOppositeEdge(viewport);
                        }
                    });

                auto setKeyboardInput = [&render,&mouseAcitve](const arg_keyboard& arg)
                {
                    if (mouseAcitve)
                        render.setKeyboardInput(arg.key);
                };

                fm.events().key_char(setKeyboardInput);

                timer tmr;

                #ifdef IMAGERT_WINDOWS
                    tmr.elapse([&render] {
                        auto hwnd = render.m_hwnd;
                        RECT r;
                        ::GetClientRect(hwnd, &r);
                        ::InvalidateRect(hwnd, &r, FALSE);
                        });
                #elif defined(IMAGERT_LINUX)
                    tmr.elapse([&] {
                        auto wd = reinterpret_cast<Window>(fm.native_handle());
                        auto disp = const_cast<Display*>(reinterpret_cast<const Display*>(API::x11::get_display()));

                        XClearArea(disp, wd, 0, 0, 1, 1, true);
                        XFlush(disp);

                        });
                #endif

                tmr.interval((std::chrono::milliseconds)0);
                tmr.start();

                fm.show();
             
                exec();
            }

       
            void GUI::updateTreeBox(nana::treebox& tree) {

                for (auto n : _scene.getModelsToDraw()) {
                    for (auto r : n->meshes) {
                        std::string t = r.meshName;
                        auto objnode = tree.insert(t, t);
                    }
                }

            }

            void GUI::menubar() {
                mn.push_back("&File");
                mn.push_back("Edit");
               
                mn.push_back("Help");
                mn.at(0).append("&New Ctrl+F3", [](nana::menu::item_proxy& ip) {});
                mn.at(0).append(u8"&Open F3", [this](nana::menu::item_proxy& ip) {
                    fileBoxOpenProject(_scene);
                    });
                mn.at(0).append("Save F2", [](nana::menu::item_proxy& ip) {});
                mn.at(0).append("Save as... Ctrl+F2", [](nana::menu::item_proxy& ip) {});
                mn.at(0).append_splitter();
                mn.at(0).append(u8"&Exit F12", [](nana::menu::item_proxy& ip) {nana::API::exit_all(); });

                mn.at(1).append("Undo Ctrl+Z", [](nana::menu::item_proxy& ip) {});
                mn.at(1).append("Redo Ctrl+Y", [](nana::menu::item_proxy& ip) {});
                mn.at(1).append_splitter();
                mn.at(1).append("Cut Ctrl+X", [](nana::menu::item_proxy& ip) {});
                mn.at(1).append("Copy Ctrl+C", [](nana::menu::item_proxy& ip) {});
                mn.at(1).append("Paste Ctrl+V", [](nana::menu::item_proxy& ip) {});
                mn.at(1).append(u8"Delete Del", [](nana::menu::item_proxy& ip) {});
                mn.at(1).append_splitter();
                mn.at(1).append("Select ALL Ctrl+A", [](nana::menu::item_proxy& ip) {});
                mn.at(2).append("About", [](nana::menu::item_proxy& ip)
                    {
                        using namespace nana;
                        nana::form infoScreen(nana::API::make_center(800, 600));
                        infoScreen.caption("About ImageRT");
                        label lb{ infoScreen, rectangle{ 0, 0, 800, 600 } };
                        lb.format(true);
                        std::string sqlVer = sqlite3_libversion();
                        lb.caption("ImageRT version 0.0.1 pre alpha pre release <bold color=blue  url=\"https://gitlab.com/marwik15/imagert\">\https://gitlab.com/marwik15/imagert\</> \nSQLlite version : "+ sqlVer);
                        
                        infoScreen.div("<><vfit=200 text><>");
                        infoScreen["text"] << lb;
                     
                        infoScreen.collocate();
                        infoScreen.focus();
                        infoScreen.modality();
                        infoScreen.show();
                    });
            }
       
    } //namespace Gui
} //namespace imageRT