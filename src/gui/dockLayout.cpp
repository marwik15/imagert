#pragma once

/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/

#include <gui/dockLayout.hpp>

namespace imageRT {
    namespace Gui {
  
            dockLayout::dockLayout(nana::panel<true>& container) {
                setContainerPos(container);
                setContainerSize(container);
                id = 0;

                auto resizeEvent = [&]() mutable
                {
                    auto prevSize = containerSize.width;
                    setContainerSize(container);
                    auto newSize = containerSize.width;
                    collocate();
                };

                container.events().resizing(resizeEvent);
                container.events().resized(resizeEvent);
            }
            void dockLayout::collocate() {
                fillWidth();
                makeLine();
            }
            void dockLayout::showAll() {
                for (auto n : m_panels) {
                    n.panel->show();
                }
            }
      
            void dockLayout::addpanel(nana::nested_form* p, PanelInfo panelInfo = PanelInfo()) {
                PanelQueue pq;
                pq.order = id;
                pq.panel = p;
                pq.panelInfo = panelInfo;
                pq.panelInfo.defX = p->size().width;
                pq.panelInfo.defY = p->size().height;
                m_panels.push_back(pq);
                id++;
            }

            void dockLayout::panelsize(){
                auto sizepanel = m_panels[0].panel->size();
                std::cout << sizepanel.width;
            }
            void dockLayout::fillWidth() {
                unsigned int totalWidth = 0;
                unsigned int  w = containerSize.width / m_panels.size();

                for (auto i : m_panels) {
                    if (!i.panelInfo.noData) {
                        if (i.panelInfo.resizable) {
                            continue;
                        }
                        totalWidth += i.panel->size().width;
                    }
                }

                if (totalWidth < containerSize.width) { //stretch panels to container
                     w = containerSize.width - totalWidth;
                }

                for (auto i : m_panels) {

                    if (!i.panelInfo.noData) {
                        if (i.panelInfo.resizable) {
                           continue;
                        }
                        if (w <= i.panelInfo.minX) {
                           w = i.panelInfo.minX;
                        }
                      
                        i.panel->size(nana::size(w, i.panelInfo.defY));
                    }
                }
            }
            void dockLayout::makeLine() {
                nana::size xOffset;
                auto _id = 0;

                for (auto i : m_panels) {
                    if (_id == 0) {
                        m_panels[_id].panel->move(containerPos);
                    } else {
                        auto prevWidth = m_panels[_id - 1].panel->size().width;
                        xOffset.width += prevWidth;
                        panelMove(_id, containerPos.x + xOffset.width, containerPos.y);
                    }
                    _id++;
                }
            }
     
            void dockLayout::panelMove(int id, int x, int y) {
                m_panels[id].panel->move(x, y);
            }

            void dockLayout::setContainerPos(nana::panel<true>& container) {
                containerPos = container.pos();
            }

            void dockLayout::setContainerSize(nana::panel<true>& container) {
                containerSize = container.size();
            }

    } //namespace Gui
} //namespace imageRT