#pragma once

/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/
#include <renderer/vulkan/vulkan.hpp>

namespace imageRT {
	namespace Renderer {
		namespace Vulkan {
			
			#ifdef ImageRT_VULKAN 
			
				void VulkanRenderer::rendererInit() {
					
					if (!glfwInit())
					glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
					GLFWwindow*  window = glfwCreateWindow(640, 480, "vulkan test", NULL, NULL);

					if (glfwVulkanSupported())
					{
						
						VkApplicationInfo appInfo{};
						appInfo.sType = VK_STRUCTURE_TYPE_APPLICATION_INFO;
						appInfo.pApplicationName = "Hello Triangle";
						appInfo.applicationVersion = VK_MAKE_VERSION(1, 0, 0);
						appInfo.pEngineName = "No Engine";
						appInfo.engineVersion = VK_MAKE_VERSION(1, 0, 0);
						appInfo.apiVersion = VK_API_VERSION_1_0;

						VkInstanceCreateInfo createInfo{};
						createInfo.sType = VK_STRUCTURE_TYPE_INSTANCE_CREATE_INFO;
						createInfo.pApplicationInfo = &appInfo;

						uint32_t glfwExtensionCount = 0;
						const char** glfwExtensions;

						glfwExtensions = glfwGetRequiredInstanceExtensions(&glfwExtensionCount);

						createInfo.enabledExtensionCount = glfwExtensionCount;
						createInfo.ppEnabledExtensionNames = glfwExtensions;

						createInfo.enabledLayerCount = 0;

						if (vkCreateInstance(&createInfo, nullptr, &instance) != VK_SUCCESS) {
							std::cout << "Vulkan not supported\n";
							return;
						}
						std::cout << "Vulkan supported\n";
						uint32_t extensionCount = 0;
						vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, nullptr);
						std::vector<VkExtensionProperties> extensions(extensionCount);
						vkEnumerateInstanceExtensionProperties(nullptr, &extensionCount, extensions.data());
						std::cout << "available extensions:\n";

						for (const auto& extension : extensions) {
							std::cout << '\t' << extension.extensionName << '\n';
						}

						vkDestroyInstance(instance, nullptr);

						glfwDestroyWindow(window);

						
					}

					glfwTerminate();
				}
				bool VulkanRenderer::rendererStart()  { return false; }
				bool VulkanRenderer::rendererStop()  {
					return false;
				}
				void VulkanRenderer::destroy()  {}

				bool VulkanRenderer::rendererPause() {
					return false;
				}
			#endif // ImageRT_VULKAN
			
		} //namespace Vulkan
	} //namespace Renderer
} //namespace imageRT