#pragma once

/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/

#include <renderer/openGL/shader/shaderloader.hpp>

#ifdef IMAGERT_COMPILED_RESOURCES
    #include <cmrc/cmrc.hpp>

    CMRC_DECLARE(ImageRTresources);
#endif // IMAGERT_USE_COMPILED_RESOURCES

namespace imageRT {
    namespace Renderer {
        namespace Opengl {
         
              const char* shaderLoader::getSource(shaderType type) {
                  switch (type){
                  case imageRT::Renderer::Opengl::shaderType::vertex:
                      return vertexCode.c_str();
                      break;
                  case imageRT::Renderer::Opengl::shaderType::fragment:
                      return fragmentCode.c_str();
                      break;
                  default:
                      break;
                  }
              }
           
              void shaderLoader::LoadShaderFromResource(cmrc::file vertexFile, cmrc::file fragmentFile) {

                  #ifdef IMAGERT_USE_COMPILED_RESOURCES
                  std::string fraglslSrc;
                  std::string vertSrc;

                  for (auto re : vertexFile) {
                      vertSrc += re;
                  }

                  for (auto re : fragmentFile) {
                      fraglslSrc += re;
                  }

                  this->vertexCode = vertSrc;
                  this->fragmentCode = fraglslSrc;

                  #endif // IMAGERT_USE_COMPILED_RESOURCES
              }

            
              void shaderLoader::LoadShaderFromString(std::string vertexCode, std::string fragmentCode) {
                  this->vertexCode = vertexCode;
                  this->fragmentCode = fragmentCode;

              }
             
              void shaderLoader::Loadshader(const char* vertexPath, const char* fragmentPath) {
                   #ifdef IMAGERT_USE_COMPILED_RESOURCES

                  auto fs = cmrc::ImageRTresources::get_filesystem();

                  LoadShaderFromResource(fs.open(vertexPath), fs.open(fragmentPath));

                  #endif
                  #ifndef IMAGERT_USE_COMPILED_RESOURCES
                  LoadShaderFromFile(vertexPath, fragmentPath);
                  #endif
              }

              void shaderLoader::LoadShaderFromFile(const char* vertexPath, const char* fragmentPath) {

                  std::ifstream vShaderFile;
                  std::ifstream fShaderFile;
                  // ensure ifstream objects can throw exceptions:
                  vShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
                  fShaderFile.exceptions(std::ifstream::failbit | std::ifstream::badbit);
                  try{
                      // open files
                      vShaderFile.open(vertexPath);
                      fShaderFile.open(fragmentPath);
                      std::stringstream vShaderStream, fShaderStream;
                      // read file's buffer contents into streams
                      vShaderStream << vShaderFile.rdbuf();
                      fShaderStream << fShaderFile.rdbuf();
                      // close file handlers
                      vShaderFile.close();
                      fShaderFile.close();
                      // convert stream into string
                      this->vertexCode = vShaderStream.str();
                      // fragmentCode = fShaderStream.str();
                      this->fragmentCode = fShaderStream.str();

                  }
                  catch (std::ifstream::failure& e) {
                      std::cout << "ERROR::SHADER::FILE_NOT_SUCCESFULLY_READ" << std::endl;
                  }
              }

        }//namespace Opengl
    }//namespace Renderer
} //namespace imageRT