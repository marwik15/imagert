#pragma once

/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/

#include <renderer/openGL/shader/oglShader.hpp>


namespace imageRT {
    namespace Renderer {
        namespace Opengl {

                void Shader::use() {
                    glUseProgram(ID);
                }

                void Shader::createShader() {
                    if (getSource(shaderType::vertex) == "" || getSource(shaderType::fragment) == "") std::cout << "empty shader source\n ";

                    const char* vShaderCode = getSource(shaderType::vertex);
                    const char* fShaderCode = getSource(shaderType::fragment);

                    unsigned int vertex, fragment;

                    vertex = glCreateShader(GL_VERTEX_SHADER);
                    glShaderSource(vertex, 1, &vShaderCode, NULL);
                    glCompileShader(vertex);
                    checkCompileErrors(vertex, "VERTEX");

                    fragment = glCreateShader(GL_FRAGMENT_SHADER);
                    glShaderSource(fragment, 1, &fShaderCode, NULL);
                    glCompileShader(fragment);
                    checkCompileErrors(fragment, "FRAGMENT");

                    ID = glCreateProgram();
                    glAttachShader(ID, vertex);
                    glAttachShader(ID, fragment);
                    glLinkProgram(ID);
                    checkCompileErrors(ID, "PROGRAM");

                    glDeleteShader(vertex);
                    glDeleteShader(fragment);
                }

                bool Shader::checkCompileErrors(unsigned int shader, std::string type){
                    int success;
                    char infoLog[1024];
                    if (type != "PROGRAM")
                    {
                        glGetShaderiv(shader, GL_COMPILE_STATUS, &success);
                        if (!success)
                        {
                            glGetShaderInfoLog(shader, 1024, NULL, infoLog);
                            std::cout << "ERROR::SHADER_COMPILATION_ERROR of type: " << type << "\n" << infoLog << "\n -- --------------------------------------------------- -- " << std::endl;
                            return false;
                        }
                    }
                    else
                    {
                        glGetProgramiv(shader, GL_LINK_STATUS, &success);
                        if (!success)
                        {
                            glGetProgramInfoLog(shader, 1024, NULL, infoLog);
                            std::cout << "ERROR::PROGRAM_LINKING_ERROR of type: " << type << "\n" << infoLog << "\n -- --------------------------------------------------- -- " << std::endl;
                            return false;
                        }
                    }

                    return true;
                }
        }//namespace Opengl
    }//namespace Renderer
} //namespace imageRT