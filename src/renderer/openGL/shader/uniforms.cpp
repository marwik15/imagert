#pragma once

/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/

#include <renderer/openGL/shader/uniforms.hpp>

namespace imageRT {
    namespace Renderer {
        namespace Opengl {
               
                void Uniforms::setBool(const std::string& name, bool value) const
                {
                    glUniform1i(glGetUniformLocation(ID, name.c_str()), (int)value);
                }
               
                void Uniforms::setInt(const std::string& name, int value) const
                {
                    glUniform1i(glGetUniformLocation(ID, name.c_str()), value);
                }
             
                void Uniforms::setFloat(const std::string& name, float value) const
                {
                    glUniform1f(glGetUniformLocation(ID, name.c_str()), value);
                }
                void Uniforms::setVec2(const std::string& name, const glm::vec2& value) const
                {
                    glUniform2fv(glGetUniformLocation(ID, name.c_str()), 1, &value[0]);
                }
                void Uniforms::setVec2(const std::string& name, float x, float y) const
                {
                    glUniform2f(glGetUniformLocation(ID, name.c_str()), x, y);
                }
                void Uniforms::Uniforms::setVec3(const std::string& name, const glm::vec3& value) const
                {
                    glUniform3fv(glGetUniformLocation(ID, name.c_str()), 1, &value[0]);
                }
                void Uniforms::setVec3(const std::string& name, float x, float y, float z) const
                {
                    glUniform3f(glGetUniformLocation(ID, name.c_str()), x, y, z);
                }
                void Uniforms::setVec4(const std::string& name, const glm::vec4& value) const
                {
                    glUniform4fv(glGetUniformLocation(ID, name.c_str()), 1, &value[0]);
                }
                void Uniforms::setVec4(const std::string& name, float x, float y, float z, float w)
                {
                    glUniform4f(glGetUniformLocation(ID, name.c_str()), x, y, z, w);
                }
                void Uniforms::setMat2(const std::string& name, const glm::mat2& mat) const
                {
                    glUniformMatrix2fv(glGetUniformLocation(ID, name.c_str()), 1, GL_FALSE, &mat[0][0]);
                }
                void Uniforms::setMat3(const std::string& name, const glm::mat3& mat) const
                {
                    glUniformMatrix3fv(glGetUniformLocation(ID, name.c_str()), 1, GL_FALSE, &mat[0][0]);
                }
                void Uniforms::setMat4(const std::string& name, const glm::mat4& mat) const
                {
                    glUniformMatrix4fv(glGetUniformLocation(ID, name.c_str()), 1, GL_FALSE, &mat[0][0]);
                }
             
        }//namespace Opengl
    }//namespace Renderer
} //namespace imageRT