#pragma once

/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/

#include <renderer/openGL/oglBuffers.hpp>

namespace imageRT {
    namespace Renderer {
        namespace Opengl {
              
                void Buffers::create(const void* verts, unsigned long long size) {
                    Buffer* tmp = new Buffer;
                    tmp->create(verts, size);

                    buffs.push_back(tmp);
                }
            
        } //namespace Opengl
    } //namespace Renderer
} //namespace imageRT