#pragma once

/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/

#include <renderer/openGL/nativeOpengl.hpp>



namespace imageRT {
    namespace Renderer {
        namespace Opengl {

                void nativeOpengl::createOpenGlContext() {
                #ifdef IMAGERT_WINDOWS
                    m_hRC = EnableOpenGLWinContext(m_hDC);
                #elif defined(IMAGERT_LINUX)
                    m_glctx = EnableOpenGLX11Context(windowHandle);
                #endif
                }

                void nativeOpengl::OpenGLwindowPoiner(nana::native_window_type& windowHandle) {
                    #ifdef IMAGERT_WINDOWS
                    nativeOpenGlwindowPointerWin(windowHandle);
                    #elif defined(IMAGERT_LINUX)
                    nativeOpenGlwindowPointerX11(windowHandle);
                    #endif
                }

                void nativeOpengl::nativeSwapBuff() {
                    #ifdef IMAGERT_WINDOWS
                    glSwapBuffWin();
                    #elif defined(IMAGERT_LINUX)
                    glSwapBuffX11();
                    #endif
                }

                void nativeOpengl::nativeOpenGLcontextDestroy() {
                    #ifdef IMAGERT_WINDOWS
                    destroyContextWin();
                    #elif defined(IMAGERT_LINUX)
                    destroyContextX11();
                    #endif
                }
            
                void nativeOpengl::nativeOpenGlwindowPointerWin(nana::native_window_type& windowHandle) {
                    #ifdef IMAGERT_WINDOWS
                    auto hwnd = reinterpret_cast<HWND>(windowHandle);
                    HDC hDC = ::GetDC(hwnd);

                    m_hwnd = hwnd;
                    m_hDC = hDC;
                    #endif
                }
                void nativeOpengl::nativeOpenGlwindowPointerX11(nana::native_window_type& windowHandle) {
                    #ifdef IMAGERT_LINUX
                    auto glctx = EnableOpenGLX11Context(windowHandle);
                    m_glctx = glctx;
                    #endif
                }

                void nativeOpengl::destroyContextWin()  {
                    #ifdef IMAGERT_WINDOWS
                    wglMakeCurrent(nullptr, nullptr);
                    wglDeleteContext(m_hRC);
                    ReleaseDC(m_hwnd, m_hDC);
                    #endif
                }
                void nativeOpengl::destroyContextX11() {
                    #ifdef defined(IMAGERT_LINUX)
                    auto disp = const_cast<Display*>(reinterpret_cast<const Display*>(nana::API::x11::get_display()));
                    glXMakeCurrent(disp, None, nullptr);
                    glXDestroyContext(disp, m_glctx);
                    #endif
                }

                void nativeOpengl::glSwapBuffWin() {
                #ifdef IMAGERT_WINDOWS
                    SwapBuffers(m_hDC);
                #endif
                }

                void nativeOpengl::glSwapBuffX11() {
                #ifdef IMAGERT_LINUX
                    auto disp = const_cast<Display*>(reinterpret_cast<const Display*>(nana::API::x11::get_display()));

                    glXSwapBuffers(disp, reinterpret_cast<Window>(windowHandle));
                #endif      
                }
                
        } //namespace Opengl
    } //namespace Renderer
} //namespace imageRT