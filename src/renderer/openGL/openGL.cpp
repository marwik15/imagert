#pragma once

/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/

#include <renderer/openGL/openGL.hpp>

namespace imageRT {
	namespace Renderer {
        namespace Opengl {
            /// @brief ogl render class
        
                void openGLRenderer::deleteAll() {
                    modelsToDraw.clear();
                }

                void openGLRenderer::setModelsToDraw(std::vector<imageRT::Core::Data::Model*>& modelsToDraw) {
                    this->modelsToDraw = modelsToDraw;
                }

           
                /// @brief init opengl and glad
                /// @param windowHandle 
                void openGLRenderer::rendererInit(nana::native_window_type windowhandle) {
                    OPTICK_SET_MEMORY_ALLOCATOR(
                        [](size_t size) -> void* { return operator new(size); },
                        [](void* p) { operator delete(p); },
                        []() { /* Do some TLS initialization here if needed */ }
                    );
                    windowHandle = windowhandle; //save window handle
                    OpenGLwindowPoiner(windowHandle);

                    if (!glfwInit())
                        std::cout << "Failed to initialize GLFW" << std::endl;
                    glfwWindowHint(GLFW_VISIBLE, GLFW_FALSE);
                    glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
                    glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
                    window = glfwCreateWindow(640, 480, "Simple example", NULL, NULL);
                    glfwMakeContextCurrent(window);

                    !gladLoadGL(glfwGetProcAddress) ? gladLoaded = false : gladLoaded = true;

                    if (!gladLoaded) {
                        std::cout << "Failed to initialize GLAD, openGL can't be used" << std::endl;
                        return;
                    }

                    //nana context 
                    createOpenGlContext();

                    sh.Loadshader("resources/shaders/vert.glsl", "resources/shaders/frag.glsl");
                    sh.createShader();

                    singleColor.Loadshader("resources/shaders/vert.glsl", "resources/shaders/stencilSingleColorFS.glsl");
                    singleColor.createShader();
                    buff.create(&vertices,sizeof(vertices));

                    printOGLinfo();

                    glEnable(GL_DEPTH_TEST);
                    glDepthFunc(GL_LESS);
                    glEnable(GL_STENCIL_TEST);
                    glStencilFunc(GL_NOTEQUAL, 1, 0xFF);
                    glStencilOp(GL_KEEP, GL_KEEP, GL_REPLACE);
                }

              
                void openGLRenderer::printOGLinfo() {

                    GLint MAJOR_VERSION;
                    GLint MINOR_VERSION;
                    glGetIntegerv(GL_MAJOR_VERSION, &MAJOR_VERSION);
                    glGetIntegerv(GL_MINOR_VERSION, &MINOR_VERSION);
                    std::cout << "GL VERSION " << MAJOR_VERSION << "." << MINOR_VERSION << std::endl;
                }

            
                void openGLRenderer::drawOutline(int VAO){
                    OPTICK_GPU_EVENT("Draw outline");
                    glBindVertexArray(VAO);

                    // 1st. render pass
                    glStencilFunc(GL_ALWAYS, 1, 0xFF);
                    glStencilMask(0xFF);
                    glDrawArrays(GL_TRIANGLES, 0, 36);

                    // 2nd. render pass
                    glStencilFunc(GL_NOTEQUAL, 1, 0xFF);
                    glStencilMask(0x00);
                    glDisable(GL_DEPTH_TEST);
                    singleColor.use();

                    float scale = 1.01;
                    singleColor.setMat4("view", view);
                    singleColor.setMat4("projection", projection);
                    model = glm::scale(model, glm::vec3(scale, scale, scale));
                    singleColor.setMat4("model", model);
                    glDrawArrays(GL_TRIANGLES, 0, 36);
                    glBindVertexArray(0);
                    glStencilMask(0xFF);
                    glStencilFunc(GL_ALWAYS, 0, 0xFF);
                    glEnable(GL_DEPTH_TEST);

                }

                void openGLRenderer::renderUpdate() {
                    OPTICK_FRAME("render loop");
                    errorCheckAndPrint();

                    glClearColor(0.2f, 0.3f, 0.3f, 1.0f);
                    glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
                
                    sh.use();

                    frame += .8f;
                    projection = glm::perspective(glm::radians(camera.Zoom), (float)getViewportWitdh() / (float)getViewportHeight(), 0.1f, 1000.0f);
                    view = glm::lookAt(cameraPos, cameraPos + cameraFront, cameraUp);
                    model = glm::mat4(1.0f);
                    model = glm::rotate(model, glm::radians(frame), glm::vec3(1.0f, 0.3f, 0.5f));
                    sh.setMat4("view", view);
                    sh.setMat4("projection", projection);
                    sh.setMat4("model", model);

                    sh.setVec3("lightPos", cameraPos);
                    sh.setVec3("viewPos", cameraPos);
                    sh.setVec3("lightColor", 1.0f, 1.0f, 1.0f);
                    sh.setVec3("objectColor", .8f, 0.2f, 0.31f);

                    drawOutline(buff.buffs[0]->VAO);

                    sh.use();

                    model = glm::mat4(1.0f);
                    //model = glm::translate(model, glm::vec3(0, -5, 0));
                    model = glm::scale(model, glm::vec3(.01, .01, .01));
                    sh.setMat4("model", model);
                    OPTICK_GPU_EVENT("Draw scene");
                    for (auto n : modelsToDraw) {
                        n->Draw(sh);
                    }
                    
                    gl_swap_buf();
                }

                bool openGLRenderer::rendererStart() {
                    return false;
                }

                bool firstMouse = true;
                float yaw = -90.0f;	// yaw is initialized to -90.0 degrees since a yaw of 0.0 results in a direction vector pointing to the right so we initially rotate a bit to the left.
                float pitch = 0.0f;
                float lastX = 800.0f / 2.0;
                float lastY = 600.0 / 2.0;
                float fov = 45.0f;
                glm::vec3 cameraPos = glm::vec3(0.0f, 0.0f, 3.0f);
                glm::vec3 cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
                glm::vec3 cameraUp = glm::vec3(0.0f, 1.0f, 0.0f);

                void openGLRenderer::setMousePos(double xpos, double ypos) {

                    if (firstMouse) {
                        lastX = xpos;
                        lastY = ypos;
                        firstMouse = false;
                    }

                    float xoffset = xpos - lastX;
                    float yoffset = lastY - ypos; // reversed since y-coordinates go from bottom to top
                    lastX = xpos;
                    lastY = ypos;

                    float sensitivity = 0.1f; // change this value to your liking
                    xoffset *= sensitivity;
                    yoffset *= sensitivity;

                    yaw += xoffset;
                    pitch += yoffset;

                    // make sure that when pitch is out of bounds, screen doesn't get flipped
                    if (pitch > 89.0f)
                        pitch = 89.0f;
                    if (pitch < -89.0f)
                        pitch = -89.0f;

                    glm::vec3 front;
                    front.x = cos(glm::radians(yaw)) * cos(glm::radians(pitch));
                    front.y = sin(glm::radians(pitch));
                    front.z = sin(glm::radians(yaw)) * cos(glm::radians(pitch));
                    cameraFront = glm::normalize(front);
                }

                void openGLRenderer::setKeyboardInput(char key) {
                    float deltaTime = 1.0f;
                    float cameraSpeed = 2.5 * deltaTime;

                    if (key == 'w') 
                        cameraPos += cameraSpeed * cameraFront;
                    if (key == 's')
                        cameraPos -= cameraSpeed * cameraFront;
                    if (key == 'a')
                        cameraPos -= glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
                    if (key == 'd')
                        cameraPos += glm::normalize(glm::cross(cameraFront, cameraUp)) * cameraSpeed;
                 
                }

             
                void openGLRenderer::errorCheckAndPrint() {
                    GLenum error = glGetError();
                    switch (error) {
                    case GL_INVALID_ENUM:
                        std::cout << "GL_INVALID_ENUM\n";
                        break;
                    case GL_INVALID_VALUE:
                        std::cout << "GL_INVALID_VALUE\n";
                        break;
                    case GL_INVALID_OPERATION:
                        std::cout << "GL_INVALID_OPERATION\n";
                        break;
                    case GL_INVALID_FRAMEBUFFER_OPERATION:
                        std::cout << "GL_INVALID_FRAMEBUFFER_OPERATION\n";
                        break;
                    case GL_OUT_OF_MEMORY:
                        std::cout << "GL_OUT_OF_MEMORY\n";
                        break;
                    case GL_NO_ERROR:
                        break;

                    }
                }
                
                void openGLRenderer::setViewPortSize(int w, int h) {
                    glViewport(0, 0, w, h);
                }

              
                void openGLRenderer::gl_swap_buf(){
                    nativeSwapBuff();
                };
              
             
                void openGLRenderer::destroy(){
                    nativeOpenGLcontextDestroy();
                }

              
            
        } //namespace Opengl
	} //namespace Renderer
} //namespace imageRT