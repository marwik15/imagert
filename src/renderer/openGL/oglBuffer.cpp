#pragma once

/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/

#include <glad/gl.h>
#include <renderer/openGL/oglBuffer.hpp>

namespace imageRT {
    namespace Renderer {
        namespace Opengl {
            /// @brief ogl buffer class
        
                void Buffer::create(const void* verts, unsigned long long size){
                    glGenVertexArrays(1, &VAO);
                    glGenBuffers(1, &VBO);

                    glBindVertexArray(VAO);

                    glBindBuffer(GL_ARRAY_BUFFER, VBO);
                    glBufferData(GL_ARRAY_BUFFER, size, verts, GL_STATIC_DRAW);

                    // position attribute
                    glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 5 * sizeof(float), (void*)0);
                    glEnableVertexAttribArray(0);
                }
                
        } //namespace Opengl
    } //namespace Renderer
} //namespace imageRT