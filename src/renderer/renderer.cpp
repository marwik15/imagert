#pragma once

/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/

#include <renderer/renderer.hpp>

namespace imageRT {
	namespace Renderer {
	
			int Renderer::getViewportWitdh() {
				return this->viewportWidth;
			}
			int  Renderer::getViewportHeight() {
				return this->viewportHeight;
			}

			void Renderer::saveViewportDimentions(int viewportWidth, int viewportHeight) {
				this->viewportWidth = viewportWidth;
				this->viewportHeight = viewportHeight;
			}

	}//namespace Renderer
}//namespace imageRT