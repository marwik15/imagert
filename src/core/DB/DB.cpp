#pragma once

/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/

#include <core/DB/DB.hpp>

namespace imageRT {
	namespace DB {
        std::vector<std::vector<DBreturnedTab>>& DB::getData() {
            return returnData;
        }
         
        bool DB::openDB(std::string name) {
            if (!std::filesystem::is_empty(name)) {
                rc = sqlite3_open(name.c_str(), &db);
                return true;
            }
            return false;
        }
           
        void DB::createDB(std::string name) {
            rc = sqlite3_open(name.c_str(), &db);
        }


        void DB::sendSQLquery(std::string sql) {
            rc = sqlite3_exec(db, sql.c_str(), callback, (void*)&returnData, &err_msg);
        }

        bool DB::checkConnetionError() {
            if (rc != SQLITE_OK) {

                fprintf(stderr, "Cannot open database: %s\n", sqlite3_errmsg(db));
                sqlite3_close(db);

                return 1;
            }
        }

        void DB::closeDB() {
            sqlite3_close(db);
        }

        bool DB::checkSQLqueryError() {
            if (rc != SQLITE_OK) {

                fprintf(stderr, "SQL error: %s\n", err_msg);

                sqlite3_free(err_msg);

                return 1;
            }
        }

        int DB::callback(void* data, int argc, char** argv, char** azColName) {
             
            DBreturnedTab dbret;
            std::vector<DBreturnedTab> vec;

            std::vector<std::vector<DBreturnedTab>>* p = reinterpret_cast<std::vector<std::vector<DBreturnedTab>> *>(data);

            for (int i = 0; i < argc; i++) {
                dbret.argv = argv[i] ? argv[i] : "NULL";
                dbret.azColName = azColName[i];
                vec.push_back(dbret);
                   
            }
                
            p->push_back(vec);
                
            return 0;
        }
	} //namespace DB
} //namespace imageRT