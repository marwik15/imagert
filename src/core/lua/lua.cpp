#pragma once

/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/
#include <iostream>
#include <core/lua/lua.hpp>

namespace imageRT {
	namespace Core {
		namespace Lua {
			
			bool Lua::checkLua(lua_State *L,int r) {
				if (r != LUA_OK) {
					std::string errormsg = lua_tostring(L, -1);
					std::cout << errormsg << std::endl;
					return false;
				}
				return true;
			}

			int Lua::lua_TestFunction(lua_State* L) {
				std::cout << "c++ from lua test\n";
				return 0;
			}

			Lua::Lua(){}
			void Lua::run() {

				lua_State* L = luaL_newstate();
				luaL_openlibs(L);
				lua_register(L, "testF", &Lua::lua_TestFunction);

				if (checkLua(L, luaL_dofile(L, "test.lua"))){

					lua_getglobal(L, "a");
					if (lua_isnumber(L, -1)) {
						std::cout << "[LUA] " << (float)lua_tonumber(L, -1) << std::endl;
					}
				}
			}
		} //namespace Lua
	} //namespace Core
} //namespace imageRT