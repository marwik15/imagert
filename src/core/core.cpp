#pragma once

/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/

#include <core/core.hpp>

namespace imageRT {
	namespace Core {
		
		std::string Core::getFileExtention(std::string file) {
			std::string ret;
			for (int i = file.size(); i > 0; i--) {
				if (file[i] == '.') {
					std::string ret = file.erase(0, i);
					return ret;
				}
			}
			return ret = "";
		}
	} //namespace Core
} //namespace imageRT