#pragma once
/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/

#include <core/cursor/native/cursor_win.hpp>

namespace imageRT {
	namespace Cursor {
		/// @brief windows cursor class
	#ifdef IMAGERT_WINDOWS

		bool CursorWin::getCursorPos() {
			return GetCursorPos(&cursorPos);
		}

		bool CursorWin::getCursorVisibleWin() {
			return cursorVisible;
		}

		void CursorWin::showCursorWin(bool show) {
			cursorVisible = show;
			ShowCursor(show);
		}
		bool CursorWin::setCursorPosWin(int x, int y) {
			return SetCursorPos(x, y);
		}

		POINT CursorWin::getCursorPosWin() {
			getCursorPos();
			return cursorPos;
		}

		bool CursorWin::ClientToScreenWin(HWND  hWnd, POINT& Point) {
			return ClientToScreen(hWnd, &Point);
		}

		bool CursorWin::clipCursorWin(const RECT& clipArea) {
			return ClipCursor(&clipArea);
		}

		RECT CursorWin::makeClipAreaFromViewportWin(nana::nested_form& viewport) {
			RECT mouseClipArea;
			POINT p = { 0,0 };
			ClientToScreenWin(reinterpret_cast<HWND>(viewport.native_handle()), p);
			mouseClipArea.left = p.x;
			mouseClipArea.top = p.y;
			mouseClipArea.right = p.x + viewport.size().width;
			mouseClipArea.bottom = p.y + viewport.size().height;

			return mouseClipArea;
		}

		void CursorWin::moveCursorToOppositeEdgeWin(nana::nested_form& window) {
			auto rec = makeClipAreaFromViewportWin(window);

			if (getCursorPosWin().x >= rec.right - 2) {
				mouseXrepeated++;
				setCursorPosWin(rec.left + 2, getCursorPosWin().y);
			}
			if (getCursorPosWin().x <= rec.left) {
				mouseXrepeated--;
				setCursorPosWin(rec.right, getCursorPosWin().y);
			}
			if (getCursorPosWin().y <= rec.top) {
				mouseYrepeated++;
				setCursorPosWin(getCursorPosWin().x, rec.bottom - 2);
			}
			if (getCursorPosWin().y >= rec.bottom - 1) {
				mouseYrepeated--;
				setCursorPosWin(getCursorPosWin().x, rec.top);
			}

		}

		void CursorWin::releaseClipCursorWin() {
			ClipCursor(NULL);
		}
	#endif
	} //namespace imageRT 
} //namespace Cursor 
	