#pragma once

/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/

#include <core/cursor/cursor.hpp>

namespace imageRT {
	namespace Cursor {
		
		int Cursor::getMouseXrepeated() {
			return mouseXrepeated;
		}

		int Cursor::getMouseYrepeated() {
			return mouseYrepeated;
		}
		void Cursor::resetMouseRepeated() {
			mouseXrepeated = mouseYrepeated = 0;
		}
			
		void Cursor::showCursor(bool show) {
		#ifdef IMAGERT_WINDOWS
			showCursorWin(show);
		#endif
		}
		
		bool Cursor::getCursorVisible() {
		#ifdef IMAGERT_WINDOWS
			return getCursorVisibleWin();
		#endif
		}
		
		bool Cursor::setCursorPos(int x, int y) {
		#ifdef IMAGERT_WINDOWS
			return setCursorPosWin(x, y);
		#endif
		}
		int Cursor::getCursorPosX() {
		#ifdef IMAGERT_WINDOWS
			return getCursorPosWin().x;
		#endif
		}
		/// @brief set cursor position 
		int Cursor::getCursorPosY() {
		#ifdef IMAGERT_WINDOWS
			return getCursorPosWin().y;
		#endif
		}

		void Cursor::moveCursorToOppositeEdge(nana::nested_form& window) {
		#ifdef IMAGERT_WINDOWS
			moveCursorToOppositeEdgeWin(window);
		#endif
		}

		bool Cursor::clientToScreen(nana::nested_form& window, POINT& p) {
		#ifdef IMAGERT_WINDOWS
			return ClientToScreenWin(reinterpret_cast<HWND>(window.native_handle()), p);
		#endif
		}

		bool Cursor::clipCursor(const RECT& clipArea) {
		#ifdef IMAGERT_WINDOWS
			return clipCursorWin(clipArea);
		#endif
		}

		RECT Cursor::makeClipAreaFromViewport(nana::nested_form& viewport) {
		#ifdef IMAGERT_WINDOWS
			return makeClipAreaFromViewportWin(viewport);
		#endif
		}
		void Cursor::releaseClipCursor() {
		#ifdef IMAGERT_WINDOWS
			releaseClipCursorWin();
		#endif
		}
	} //namespace imageRT 
} //namespace Cursor 
