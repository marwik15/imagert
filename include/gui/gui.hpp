#pragma once

/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/
#include <sqlite/sqlite3.h>

#include <nana/gui.hpp>
#include <nana/gui/timer.hpp>
#include <nana/gui/screen.hpp>
#include <nana/gui/dragger.hpp>
#include <nana/gui/filebox.hpp>
#include <nana/gui/state_cursor.hpp>
#include <nana/gui/widgets/panel.hpp>
#include <nana/gui/widgets/label.hpp>
#include <nana/gui/widgets/group.hpp>
#include <nana/gui/widgets/button.hpp>
#include <nana/gui/widgets/menubar.hpp>
#include <nana/gui/widgets/treebox.hpp>
#include <nana/gui/widgets/textbox.hpp>

#include <core/cursor/cursor.hpp>
#include <gui/dockLayout.hpp>
#include <application/scene.hpp>
#include <application/internal.hpp>
#include <renderer/openGL/openGL.hpp>

#ifdef ImageRT_VULKAN
#include <renderer/vulkan/vulkan.hpp>
#endif //ImageRT_VULKAN

#include <iostream>

namespace imageRT {
    namespace Gui{
        /// @brief  Gui class.
        class GUI : public Cursor::Cursor, public imageRT::application::Internal{

            nana::form fm{ nana::API::make_center(800, 600), nana::appearance(true, true, false, true, true, true, true) };
            nana::menubar mn;
            nana::panel<true> tlsPnl; //tools panel
            imageRT::application::Scene _scene;
            nana::treebox tree;

        public:

            GUI(imageRT::application::Scene& sceneRef) {
                _scene = sceneRef;
            }

            void show();

        private:
            void updateTreeBox(nana::treebox& tree);
            void menubar();
        };
    }//namespace Gui
}//namespace imageRT