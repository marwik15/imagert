#pragma once

/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/

#include <nana/gui/widgets/panel.hpp>
#include <nana/gui/widgets/form.hpp>

#include <iostream>
#include <vector>

namespace imageRT {
    namespace Gui {
        
        struct PanelInfo {
            PanelInfo() {
                noData = true;
            }
            PanelInfo(unsigned int minX, unsigned int minY,bool resizable = false) : minX(minX) , minY(minY),
                noData(false), resizable(resizable){
            }
            std::string name;
            unsigned int minX, minY, maxX, maxY,defX,defY;
            int noData;
            bool resizable;
        };

        struct PanelQueue {
            PanelInfo panelInfo;
            nana::nested_form* panel;
            unsigned int order;
        };

        /// @brief dock layout class
        class dockLayout{
            std::vector<PanelQueue> m_panels;
            nana::point containerPos;
            nana::size containerSize;
            unsigned int id;
        public:
            dockLayout(nana::panel<true>& container);
            /// @brief arrange panels 
            void collocate();
            /// @brief show all panels
            void showAll();
            
            /// @brief add panel to dock
            /// @param p 
            /// @param panelInfo 
            void addpanel(nana::nested_form* p, PanelInfo panelInfo);

            void panelsize();
        private:
            /// @brief set panels size acording to containerSize.width
            void fillWidth();
            /// @brief set panels in line
            void makeLine();
            /// @brief move panel
            /// @param id panel id
            /// @param x 
            /// @param y 
            void panelMove(int id, int x, int y);
            void setContainerPos(nana::panel<true>& container);
            void setContainerSize(nana::panel<true>& container);

        };
    }//namespace Gui
}//namespace imageRT