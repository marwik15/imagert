#pragma once

/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/

#include <nana/gui.hpp>

#include "native/cursor_win.hpp"
#include "native/cursor_linux.hpp"

namespace imageRT {
	namespace Cursor {
		/// @brief cursor class
		class Cursor : private CursorWin, private CursorLinux {
		#ifdef IMAGERT_LINUX
			struct POINT {}; //temp struct define
			struct RECT {};
		#endif
	
		private:
		public:
			/// @brief get how many times mouse cursor has moved outside viewport
			/// @return 
			int getMouseXrepeated();
			/// @brief get how many times mouse cursor has moved outside viewport
			/// @return 
			int getMouseYrepeated();
			/// @brief reset how many times mouse cursor has moved outside viewport
			void resetMouseRepeated();
			
			/// @brief show or hide cusor
			/// @param show 
			void showCursor(bool show);
			/// @brief get cursor visibility state
			/// @return 
			bool getCursorVisible();
			
			/// @brief set cursor position 
			/// @param x 
			/// @param y 
			/// @return 
			bool setCursorPos(int x, int y);
			/// @brief set cursor position 
			int getCursorPosX();
			/// @brief set cursor position 
			int getCursorPosY();

			/// @brief move cursor to opposite edge if user hits window border
			/// @param window 
			void moveCursorToOppositeEdge(nana::nested_form& window);

			/// @brief convert client window coordinates to screen coordinates
			/// @param window 
			/// @param p pointer to POINT struct - save the result
			/// @return true if successful, false otherwise
			bool clientToScreen(nana::nested_form& window, POINT& p);

			/// @brief lock cursor in specified area
			/// @param clipArea area that cursor will be locked to
			/// @return true if successful, false otherwise
			bool clipCursor(const RECT& clipArea);

			/// @brief generate clip area coordinates from viewport
			/// @param viewport 
			/// @return RECT struct
			RECT makeClipAreaFromViewport(nana::nested_form& viewport);
			/// @brief reset clip area
			void releaseClipCursor();

		};
	}//namespace Cursor
}//namespace imageRT 