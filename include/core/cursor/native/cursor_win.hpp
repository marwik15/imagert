#pragma once

/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/

#include <core/cppDefines.hpp>

#ifdef IMAGERT_WINDOWS
#include <Windows.h>
#endif

#include <nana/gui.hpp>

namespace imageRT {
	namespace Cursor {
		/// @brief windows cursor class
		class CursorWin {
		#ifdef IMAGERT_WINDOWS
		private:
			bool cursorVisible;
			POINT cursorPos;

			bool getCursorPos();
		public:
			int mouseXrepeated = 0;
			int mouseYrepeated = 0;

			bool getCursorVisibleWin();

			void showCursorWin(bool show);
			bool setCursorPosWin(int x, int y);

			POINT getCursorPosWin();

			bool ClientToScreenWin(HWND  hWnd, POINT& Point);
			bool clipCursorWin(const RECT& clipArea);

			RECT makeClipAreaFromViewportWin(nana::nested_form& viewport);

			void moveCursorToOppositeEdgeWin(nana::nested_form& window);

			void releaseClipCursorWin();
		#endif
		};
	}
}