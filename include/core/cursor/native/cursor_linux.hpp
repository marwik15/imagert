#pragma once

/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/

#include <core/cppDefines.hpp>

namespace imageRT {
	namespace Cursor {
		/// @brief linux cursor class
		class CursorLinux{
		#ifdef IMAGERT_LINUX
		private:
			bool cursorVisible;
			
		public:
			int mouseXrepeated = 0;
			int mouseYrepeated = 0;
			
		#endif
		};
	}
}