#pragma once

/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/

#include "config.h"

//Windows
#if defined(_WIN32) || defined(__WIN32__) || defined(WIN32)	
#define IMAGERT_WINDOWS

//gnu/linux
#elif defined(linux) || defined(__linux) && !defined(APPLE)	//Linux
#define IMAGERT_LINUX

#else
static_assert(false, "Only Windows and Linux are supported");
#endif

#if IMAGERT_USE_COMPILED_RESOURCES == 1
#define IMAGERT_COMPILED_RESOURCES
#endif

#if ImageRT_VULKAN_ENABLED == 1
#define ImageRT_VULKAN
#endif