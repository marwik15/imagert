#pragma once

/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/

#include <sqlite/sqlite3.h>
#include <filesystem>
#include <vector>

namespace imageRT {
	namespace DB {

        struct DBreturnedTab {
            std::string azColName;
            std::string  argv;
        };
     
		/// @brief database class
		class DB  {
		private:
            sqlite3* db;
            char* err_msg = 0;
            int rc;
            std::vector<std::vector<DBreturnedTab>> returnData;
		public:

            std::vector<std::vector<DBreturnedTab>>& getData();

            /// @brief open existing DB
            /// @param name 
            /// @return 
            bool openDB(std::string name);
            /// @brief create new DB
            /// @param name 
            void createDB(std::string name);

            void sendSQLquery(std::string sql);

            bool checkConnetionError();

            void closeDB();

            bool checkSQLqueryError();

        private:
            static int callback(void* data, int argc, char** argv, char** azColName);

		};
	}//namespace DB
}//namespace imageRT 