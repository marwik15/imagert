#pragma once

/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/

#include <string>

namespace imageRT {
	namespace Core {
		class Core {
		private:

		public:
			/// @brief get last characters from string until . (dot) is encountered
			/// @return file extention or empty string if . (dot) not found
			std::string getFileExtention(std::string file);

		};
	}
}