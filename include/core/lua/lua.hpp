#pragma once

/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/


extern "C" {
#include <lua/lua-5.4.1/include/lua.h>
#include <lua/lua-5.4.1/include/lauxlib.h>
#include <lua/lua-5.4.1/include/lualib.h>
}
#include <core/core.hpp>

namespace imageRT {
	namespace Core {
		namespace Lua {
			class Lua : public imageRT::Core::Core {
			private:
				/// @brief check Lua state
				/// @param L 
				/// @param r 
				/// @return 
				bool checkLua(lua_State* L, int r);

				static int lua_TestFunction(lua_State* L);
			public:
				Lua();
				/// @brief run Lua file
				void run();
			};
		}//namespace Lua
	}//namespace Core
}//namespace imageRT