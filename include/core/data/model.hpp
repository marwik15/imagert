#pragma once

#include <glm/glm.hpp>
#include <glm/gtc/matrix_transform.hpp>
#include <assimp/scene.h>
#include <stb/stb_image.h>
#include <assimp/Importer.hpp>
#include <assimp/postprocess.h>

#include <renderer/openGL/shader/oglShader.hpp>

#include "mesh.hpp"

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <map>
#include <vector>
using namespace std;

namespace imageRT {
    namespace Core {
        namespace Data {

            //proudly stolen from learnopengl.com
            unsigned int TextureFromFile(const char* path, const string& directory, bool gamma = false); //static cos : error LNK2005 struct already defined in .obj file

            class Model {
    
            public:
                // model data 
                vector<Texture> textures_loaded;	// stores all the textures loaded so far, optimization to make sure textures aren't loaded more than once.
                vector<Mesh>    meshes;
                string directory;
                bool gammaCorrection;
        
                Model();

                // constructor, expects a filepath to a 3D model.
                Model(string const& path, bool gamma);
        
                // draws the model, and thus all its meshes
                void Draw(imageRT::Renderer::Opengl::Shader& shader);
        
                // loads a model with supported ASSIMP extensions from file and stores the resulting meshes in the meshes vector.
                void loadModel(string const& path);
            private:
                // processes a node in a recursive fashion. Processes each individual mesh located at the node and repeats this process on its children nodes (if any).
                void processNode(aiNode* node, const aiScene* scene);
        
                Mesh processMesh(aiMesh* mesh, const aiScene* scene);
        
                // checks all material textures of a given type and loads the textures if they're not loaded yet.
                // the required info is returned as a Texture struct.
                vector<Texture> loadMaterialTextures(aiMaterial* mat, aiTextureType type, string typeName);
            };

        }//namespace Data
    }//namespace Core
}//namespace imageRT