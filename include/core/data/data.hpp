#pragma once

/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/

#include <glm/glm.hpp>

namespace imageRT {
	namespace Core {
		namespace Data {
            struct Vertex {
                glm::vec3 Position;
                glm::vec3 Normal;
                glm::vec2 TexCoords;
                glm::vec3 Tangent;
                glm::vec3 Bitangent;
            };

            struct Texture {
                unsigned int id;
                std::string type;
                std::string path;
            };

		} //namespace Data
	} //namespace Core
}//namespace imageRT