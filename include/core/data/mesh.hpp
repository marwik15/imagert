#pragma once

/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/

#include <vector>
#include <string>
#include "data.hpp"
#include <renderer/openGL/shader/oglShader.hpp>

namespace imageRT {
    namespace Core {
        namespace Data {

            //proudly stolen from learnopengl.com
            class Mesh {
            public:
                // mesh Data
                std::vector<Vertex>       vertices;
                std::vector<unsigned int> indices;
                std::vector<Texture>      textures;
                std::string meshName;
                unsigned int VAO;

                // constructor
                Mesh(std::vector<Vertex> vertices, std::vector<unsigned int> indices, std::vector<Texture> textures, std::string meshName);

                // render the mesh
                void Draw(imageRT::Renderer::Opengl::Shader& shader);
            private:
                // render data 
                unsigned int VBO, EBO;

                // initializes all the buffer objects/arrays
                void setupMesh();
               
            };
        }//namespace Data
    }//namespace Core
}//namespace imageRT