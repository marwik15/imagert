#pragma once

/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/

#include <iostream>
#include <core/cppDefines.hpp>
#include <renderer/renderer.hpp>

#ifdef ImageRT_VULKAN
	#include <vulkan/vulkan.hpp>
#endif

namespace imageRT {
	namespace Renderer {
		namespace Vulkan {
			class VulkanRenderer : public Renderer {
			#ifdef ImageRT_VULKAN 

			private:
				VkInstance instance;
			public:
				VulkanRenderer() {}
				void rendererInit() override;
				bool rendererStart() override;
				bool rendererStop() override;
				void destroy() override;

				bool rendererPause();
			#endif // ImageRT_VULKAN
			};
		} //namespace Vulkan
	} //namespace Renderer
} //namespace imageRT