#pragma once

/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/
#include <nana/gui.hpp>
#include <core/cppDefines.hpp>

#ifdef IMAGERT_WINDOWS
    #include <windows.h>
    #pragma comment(lib, "opengl32.lib")
#elif defined(IMAGERT_LINUX)
    #include <GL/glx.h>
    #include <X11/Xlib.h>
#endif


namespace imageRT {
    namespace Renderer {
        namespace Opengl {

        #ifdef IMAGERT_WINDOWS
           static HGLRC EnableOpenGLWinContext(HDC hDC) //static cos : error LNK2005 struct already defined in .obj file
            {
                PIXELFORMATDESCRIPTOR pfd;

                memset(&pfd, 0, sizeof pfd);
                pfd.nSize = sizeof(pfd);
                pfd.nVersion = 1;
                pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
                pfd.iPixelType = PFD_TYPE_RGBA;
                pfd.cColorBits = 24;
                pfd.cDepthBits = 16;
                pfd.cStencilBits = 8;
                pfd.iLayerType = PFD_MAIN_PLANE;

                auto iFormat = ChoosePixelFormat(hDC, &pfd);
                SetPixelFormat(hDC, iFormat, &pfd);

                HGLRC hRC = wglCreateContext(hDC);
                wglMakeCurrent(hDC, hRC);
                return hRC;
            }
        #elif defined(IMAGERT_LINUX)
            GLXContext EnableOpenGLX11Context(nana::native_window_type wd)
            {
                GLint attr[] = { GLX_RGBA, GLX_DEPTH_SIZE, 24, GLX_DOUBLEBUFFER, None };
                auto disp = const_cast<Display*>(reinterpret_cast<const Display*>(nana::API::x11::get_display()));
                auto vi = glXChooseVisual(disp, 0, attr);

                auto glctx = glXCreateContext(disp, vi, nullptr, GL_TRUE);
                glXMakeCurrent(disp, reinterpret_cast<Window>(wd), glctx);
                return glctx;
            }
        #endif

            /// @brief native opengl functions (context,swap buffer)
            class nativeOpengl {
            private:

            public:
            #ifdef IMAGERT_WINDOWS
                HDC m_hDC;
                HWND m_hwnd;
                HGLRC m_hRC;
            #elif defined(IMAGERT_LINUX)
                GLXContext m_glctx;
            #endif

                // move to private, create set and get for that
                nana::native_window_type windowHandle;

                void createOpenGlContext();


                /// @brief window data, saves windowHandle
                /// @param windowHandle 
                void OpenGLwindowPoiner(nana::native_window_type& windowHandle);

                /// @brief swap native opengl context
                void nativeSwapBuff();

                /// @brief destroy native opengl context
                void nativeOpenGLcontextDestroy();

            private:
                /// @brief save window info
                void nativeOpenGlwindowPointerWin(nana::native_window_type& windowHandle);
                /// @brief save window info
                void nativeOpenGlwindowPointerX11(nana::native_window_type& windowHandle);

                /// @brief destroy opengl windows context
                void destroyContextWin();
            
                /// @brief destroy opengl X11 context
                void destroyContextX11();

                /// @brief swap opengl win buffor
                void glSwapBuffWin();;

                /// @brief swap opengl X11 buffor
                void glSwapBuffX11();
                
            };
        }//namespace Opengl
    }//namespace Renderer
}//namespace imageRT