#pragma once

/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/

#include <glad/gl.h>

namespace imageRT {
    namespace Renderer {
        namespace Opengl {
            /// @brief ogl buffer class
            class Buffer {
            public:

                unsigned int VBO, VAO;
                /// @brief create and bind openGL buffer
                /// @param verts 
                /// @param size 
                void create(const void* verts, unsigned long long size);
                
            };
        }//namespace Opengl
    }//namespace Renderer
} //namespace imageRT