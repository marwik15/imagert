#pragma once

/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <glad/gl.h>
#include "uniforms.hpp"
#include "shaderloader.hpp"
#include <core/cppDefines.hpp>

namespace imageRT {
    namespace Renderer {
        namespace Opengl {
            /// @brief ogl shader class
            class Shader : public shaderLoader,  public Uniforms {
            public:
                /// @brief simple shader class
                Shader() {
                }

                /// @brief use current shader
                void use();

                /// @brief create shader 
                /// @warning use LoadShaderFromXXX to load shader code\n 
                /// see LoadShaderFromFile(), LoadShaderFromResource(), LoadShaderFromString()
                void createShader();

            private:
                /// @brief check for compile errors
                /// @param shader 
                /// @param type 
                /// @return true if no errors, false if something went wrong
                bool checkCompileErrors(unsigned int shader, std::string type);
            };
        }//namespace Opengl
    }//namespace Renderer
} //namespace imageRT