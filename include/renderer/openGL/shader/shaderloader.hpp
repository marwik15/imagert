#pragma once

/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include <core/cppDefines.hpp>


#ifdef IMAGERT_COMPILED_RESOURCES
    #include <cmrc/cmrc.hpp>

    CMRC_DECLARE(ImageRTresources);
#endif // IMAGERT_USE_COMPILED_RESOURCES
#include <glad/gl.h>
namespace imageRT {
    namespace Renderer {
        namespace Opengl {
            enum class shaderType {
                vertex,
                fragment
            };
        
            /// @brief ogl shader loader class
          class shaderLoader {
          private:
              std::string vertexCode;
              std::string fragmentCode;
          public:
              /// @brief get shader source code
              /// @param type shaderType
              /// @warning doesn't check if empty shader source 
              /// @return 
              const char* getSource(shaderType type);
              /// @brief load shader source code from cmake resource compiler
              /// @param[in] vertexFile fs.open("resources/shaders/vertexFile.glsl")
              /// @param[in] fragmentFile fs.open("resources/shaders/fragmentFile.glsl")
              void LoadShaderFromResource(cmrc::file vertexFile, cmrc::file fragmentFile);

              /// @brief load shader source code from string 
              /// @param[in] vertexCode 
              /// @param[in] fragmentCode
              void LoadShaderFromString(std::string vertexCode, std::string fragmentCode);

              /// @brief (auto) load shader from resource or file
              /// @param vertexPath 
              /// @param fragmentPath 
              void Loadshader(const char* vertexPath, const char* fragmentPath);

              /// @brief load shader source code from file
              /// @param vertexPath 
              /// @param fragmentPath 
              void LoadShaderFromFile(const char* vertexPath, const char* fragmentPath);

            };
        }//namespace Opengl
    }//namespace Renderer
} //namespace imageRT