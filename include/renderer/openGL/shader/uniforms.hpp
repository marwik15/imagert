#pragma once

/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/

#include <string>
#include <fstream>
#include <sstream>
#include "uniforms.hpp"
#include <core/cppDefines.hpp>
#include <glad/gl.h>
#include <glm/glm.hpp>

namespace imageRT {
    namespace Renderer {
        namespace Opengl {
            /// @brief ogl set uniform class
            class Uniforms {
            public:
                Uniforms() {}

                /// @brief program ID
                unsigned int ID;

                /// @brief set bool uniform
                /// @param name 
                /// @param value 
                void setBool(const std::string& name, bool value) const;
                /// @brief set int uniform
                /// @param name 
                /// @param value 
                void setInt(const std::string& name, int value) const;
                /// @brief set float uniform
                /// @param name 
                /// @param value 
                void setFloat(const std::string& name, float value) const;
                void setVec2(const std::string& name, const glm::vec2& value) const;
                void setVec2(const std::string& name, float x, float y) const;
                void setVec3(const std::string& name, const glm::vec3& value) const;
                void setVec3(const std::string& name, float x, float y, float z) const;
                void setVec4(const std::string& name, const glm::vec4& value) const;
                void setVec4(const std::string& name, float x, float y, float z, float w);
                void setMat2(const std::string& name, const glm::mat2& mat) const;
                void setMat3(const std::string& name, const glm::mat3& mat) const;
                void setMat4(const std::string& name, const glm::mat4& mat) const;
             
            };
        }//namespace Opengl
    }//namespace Renderer
} //namespace imageRT