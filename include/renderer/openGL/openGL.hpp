#pragma once
#include <core/cppDefines.hpp>
#include <optick/src/optick.h>
/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/

#include <iostream>
#include <glm/glm.hpp>
#include <nana/gui.hpp>
#include <glm/gtc/matrix_transform.hpp>

#include "oglBuffers.hpp"
#include "../renderer.hpp"
#include "nativeOpengl.hpp"
#include <core/data/model.hpp>
#include <renderer/camera.hpp>
#include "shader/oglShader.hpp"

namespace imageRT {
	namespace Renderer {
        namespace Opengl {
            /// @brief ogl render class
            class openGLRenderer : public Renderer, public nativeOpengl {
            private:
                Shader sh, singleColor;
                Buffers buff;
              
                GLFWwindow* window;
                bool gladLoaded;
                Camera camera;
                float frame = 0;
                int mouseX, mouseY;

              
                std::vector<imageRT::Core::Data::Model*> modelsToDraw;
            
                glm::mat4 projection;
                glm::mat4 view;
                glm::mat4 model;
                float vertices[1512] = {
                    -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,
                     0.5f, -0.5f, -0.5f,  1.0f, 0.0f,
                     0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
                     0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
                    -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
                    -0.5f, -0.5f, -0.5f,  0.0f, 0.0f,

                    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
                     0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
                     0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
                     0.5f,  0.5f,  0.5f,  1.0f, 1.0f,
                    -0.5f,  0.5f,  0.5f,  0.0f, 1.0f,
                    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,

                    -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
                    -0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
                    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
                    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
                    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
                    -0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

                     0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
                     0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
                     0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
                     0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
                     0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
                     0.5f,  0.5f,  0.5f,  1.0f, 0.0f,

                    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,
                     0.5f, -0.5f, -0.5f,  1.0f, 1.0f,
                     0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
                     0.5f, -0.5f,  0.5f,  1.0f, 0.0f,
                    -0.5f, -0.5f,  0.5f,  0.0f, 0.0f,
                    -0.5f, -0.5f, -0.5f,  0.0f, 1.0f,

                    -0.5f,  0.5f, -0.5f,  0.0f, 1.0f,
                     0.5f,  0.5f, -0.5f,  1.0f, 1.0f,
                     0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
                     0.5f,  0.5f,  0.5f,  1.0f, 0.0f,
                    -0.5f,  0.5f,  0.5f,  0.0f, 0.0f,
                    -0.5f,  0.5f, -0.5f,  0.0f, 1.0f
                };
            public:
                void deleteAll();

                void setModelsToDraw(std::vector<imageRT::Core::Data::Model*>& modelsToDraw);

                openGLRenderer(): gladLoaded(NULL),mouseX(0),mouseY(0){}
                ~openGLRenderer() {}

                /// @brief init opengl and glad
                /// @param windowHandle 
                void rendererInit(nana::native_window_type windowhandle) override;
                /// @brief print opengl info
                void printOGLinfo();

                /// @brief draw outline around selected object
                /// @param VAO 
                void drawOutline(int VAO);

                /// @brief main loop - opengl drawing functions
                void renderUpdate();

                bool rendererStart() override;

                bool firstMouse = true;
                float yaw = -90.0f;	// yaw is initialized to -90.0 degrees since a yaw of 0.0 results in a direction vector pointing to the right so we initially rotate a bit to the left.
                float pitch = 0.0f;
                float lastX = 800.0f / 2.0;
                float lastY = 600.0 / 2.0;
                float fov = 45.0f;
                glm::vec3 cameraPos = glm::vec3(0.0f, 0.0f, 3.0f);
                glm::vec3 cameraFront = glm::vec3(0.0f, 0.0f, -1.0f);
                glm::vec3 cameraUp = glm::vec3(0.0f, 1.0f, 0.0f);

                void setMousePos(double xpos, double ypos);

                void setKeyboardInput(char key);

                /// @brief check opengl errors
                void errorCheckAndPrint();
                /// @brief opengl view port size
                /// @param w widht
                /// @param h height
                void setViewPortSize(int w, int h);

                /// @brief swap opengl buffor
                void gl_swap_buf();
              
                /// @brief destroy opengl context
                void destroy() override;

             
                
            };
        }//namespace Opengl
	}//namespace Renderer
}//namespace imageRT