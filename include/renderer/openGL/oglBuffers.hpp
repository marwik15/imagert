#pragma once

/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/

#include <renderer/openGL/oglBuffer.hpp>
#include <vector>

namespace imageRT {
    namespace Renderer {
        namespace Opengl {
            /// @brief ogl buffer storage class
            class Buffers  {
            private:
            public:
                std::vector<Buffer*> buffs;
                /// @brief create buffer object
                /// @param verts 
                /// @param size 
                void create(const void* verts, unsigned long long size);

                ~Buffers() {
                    for (auto i : buffs) {
                        delete i;
                    }
                }
                
            };
        }//namespace Opengl
    }//namespace Renderer
} //namespace imageRT