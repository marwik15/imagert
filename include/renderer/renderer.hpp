#pragma once

/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/

#include <glad/gl.h>
#include <GLFW/glfw3.h> 
#include <nana/gui.hpp>
namespace imageRT {
	namespace Renderer {
		class Renderer {
		private:
			int viewportWidth, viewportHeight;
		public:
			int getViewportWitdh();
			int getViewportHeight();

			Renderer(): viewportWidth(1), viewportHeight(1){}

			void saveViewportDimentions(int viewportWidth, int viewportHeight);

			virtual void rendererInit(nana::native_window_type windowHandle) {}

			virtual void rendererInit() {}
			virtual bool rendererStart() { return false;}
			virtual bool rendererStop() {return false;}
			virtual void destroy() {}
			virtual bool rendererPause() {return false;}
		};
	}//namespace Renderer
}//namespace imageRT