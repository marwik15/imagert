#pragma once

/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/

#include <core/core.hpp>
#include <application/scene.hpp>
#include <nana/gui/filebox.hpp>

namespace imageRT {
	namespace application {
		class Internal : public Core::Core{
		private:
		public:
			Internal(){}
			void fileBoxOpenProject(application::Scene& scene);
		};
	}//namespace application
}//namespace imageRT