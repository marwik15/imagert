#pragma once

/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/

#include <iostream>
#include <gui/gui.hpp>
#include <core/lua/lua.hpp>
#include <application/scene.hpp>

namespace imageRT {
	namespace application{
		/// @brief aplication class
		class Application {
		private:
			std::vector<Scene> _scenes;
		public:
			Application(){}
			/// @brief hide of show system console
			/// @param bool show 
			/// @warning windows only
			void console(bool show);
			/// @brief gui start (main loop)
			void start();

		};
	}//namespace Application
}//namespace imageRT