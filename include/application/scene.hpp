#pragma once

/********************************************
This file is a part of ImageRT
a open source project released
under GNU GENERAL PUBLIC LICENSE Version 3.
See "LICENSE" or visit https://www.fsf.org/
(C) marwik15
*******************************************/

#include <iostream>
#include <core/DB/DB.hpp>
#include <core/data/model.hpp>
#include <optick/src/optick.h>
namespace imageRT {
	namespace application{
		/// @brief Scene class
		class Scene {
		private:
			std::string name;
			std::vector<std::vector<imageRT::DB::DBreturnedTab>> fileList;
			std::vector<imageRT::Core::Data::Model*> modelsToDraw;
			imageRT::DB::DB db;
		public:

			~Scene();

			void setFilelist(std::vector<std::vector<imageRT::DB::DBreturnedTab>>& fileList);

			std::vector<std::vector<imageRT::DB::DBreturnedTab>>& getFileList();
			/// @brief add model to draw obejct list
			/// @param mdl 
			void addModelToDraw(imageRT::Core::Data::Model* mdl);
			/// @brief add to the scene 3D object from supported file type
			/// @param path 
			void addObjectToProject(std::string path);
			/// @brief get models used to draw current scene
			/// @return 
			std::vector<imageRT::Core::Data::Model*> getModelsToDraw();
			/// @brief load ImageRT project from database 
			/// @param path 
			void openProject(std::string  path);
			/// @brief close database
			void exit();
		};
	}//namespace Application
}//namespace imageRT